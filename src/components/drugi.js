import React from 'react';

class Drugi extends React.Component {

  state = {
    imie: "Krystian",
    nazwisko: "",
    data: new Date()
  }

  klik = (e) => {
    this.setState(
      { imie: "Jerzy" },
      () => console.log('callback')
    )
  }

  componentDidMount() {
    console.log('poczatek');
    this.setState({ nazwisko: this.props.nazwisko })
    this.inter = setInterval(() => {
      this.setState({ data: new Date()})
    }, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.inter)
    console.log('koniec componentu');
  }

  zmiana = (e) => {
    this.setState({ nazwisko: e.target.value})
  }

  render() {
    return (
      <div>
        <h1>{this.state.data.toISOString()}</h1>
        <h2>To nasz drugi component { this.state.imie }</h2>
        <h2 onClick = { this.klik }>
          To nasz drugi component { this.state.nazwisko }
          <input type="text" onChange={ this.zmiana } />
        </h2>
      </div>
    )
  }
}

export default Drugi