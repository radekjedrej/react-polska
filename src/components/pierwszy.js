import React, {useState, useEffect} from 'react';
// import { CtxConsumer } from '../index'
import styled from 'styled-components'

function Pierwszy (props) {
  const [kolory, setKolory] = useState([])

  useEffect(() => {
    fetch("https://reqres.in/api/unknown")
    .then((res) => res.json() )
    .then((res) => setKolory(res.data))
    .catch((err) => console.error(err))
  }, [])

  const h3styl = {
    fontSize: '40px'
  }

  const NaszPar = styled.p`
    background-color: blue;
    display: inline;
  `

  return (
    <div>
      { kolory.map( (kolor, index) => {
        return (
          <h3 key={index} style={{color: kolor.color}} className="nasza">{kolor.name}</h3>
        )
      }) }
      <NaszPar>Wpisze sobie linijke tekstu</NaszPar>
    </div>
  )
}

export default Pierwszy