import React, { createContext } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Pierwszy from './components/pierwszy'
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter } from 'react-router-dom'
import './App.css'

const context = createContext()
const CtxProvider = context.Provider
export const CtxConsumer = context.Consumer

const user = {
  name: "Krystina",
  zalogowany: true
}

const routing = (
  <BrowserRouter>
    <CtxProvider value={{user: user}}>
      <div>
        <Route exact  path="/" component={App} />
        <Route path="/pierwszy" component={Pierwszy} />
      </div>
    </CtxProvider>
  </BrowserRouter>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
